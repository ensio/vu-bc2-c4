local ruReconGadget1Guid = Guid('5B06BF90-DD48-42ED-B7C3-F875533E6B61')
local ruReconXP4Gadget1Guid = Guid('57F6F768-ECC7-48C5-8756-75D8A9CD0B46')
local usReconGadget1Guid = Guid('67484DF8-C5AC-4818-A10D-6D20E30FCF4E')
local usReconXP4Gadget1Guid = Guid('3814AAFF-9C73-42C7-B8F1-70F99324CA9F')

Events:Subscribe('Partition:Loaded', function(partition)
	for _, instance in pairs(partition.instances) do
		if instance.instanceGuid == Guid('6CF717B6-188A-4AE7-A1D2-CC1A2333C0D7') then
			local firingFunctionData = FiringFunctionData(instance)
			firingFunctionData:MakeWritable()
			firingFunctionData.shot.initialSpeed.z = 6.0
			firingFunctionData.shot.initialSpeed.y = 1.0
			firingFunctionData.shot.inheritWeaponSpeedAmount = 0.6 -- player movement matters more
		end	
			
		if instance.instanceGuid == ruReconGadget1Guid
		or instance.instanceGuid == ruReconXP4Gadget1Guid
		or instance.instanceGuid == usReconGadget1Guid
		or instance.instanceGuid == usReconXP4Gadget1Guid then			
			local gadgets = CustomizationUnlockParts(instance);
			local c4 = UnlockAssetBase(ResourceManager:SearchForInstanceByGuid(Guid('1122B462-64B1-4AE6-8ED4-CEA3BF1BDFEF')));
			gadgets:MakeWritable();
			gadgets.selectableUnlocks:insert(6, c4); -- add before None option
		end
	end	
end)